import React, { Component } from 'react'
import './controlPanel'
import TopBar from "./controlPanel";
import DataBlock from './dataBlock';

class Widget extends Component {

    constructor(props) {
        super(props);
        this.state = {
            opened: true,
            fullscreen: false
        }
    }

    hideWidget = ()=>{
        this.setState(prevState=>({
            opened: !prevState.opened
        }));
    };

    resizeWidget = ()=>{
        this.setState(prevState=>({
            fullscreen: !prevState.fullscreen
        }));
    };

    clickHandler = (e)=> {
        switch (e) {
            case '_' : {
                this.hideWidget();
                break;
            }
            case '[]' : {
                this.resizeWidget();
                break;
            }
            case 'X' : {
                this.props.onClose(this.props.id);
            }
        }
    };


    render() {

        let classList = `widget ${this.state.fullscreen ? 'fullscreen' : ''} ${this.state.opened ? '' : 'widget-hidden'}`;

        return (
            <div className={classList}>
                <TopBar onClick={this.clickHandler}/>
                {this.state.opened && <DataBlock  path={`http://localhost:5000/${this.props.task}?id=${this.props.id}`}/>}
            </div>
        );
    }
}

export default Widget;