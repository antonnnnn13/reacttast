import React, { Component } from 'react'

class DataBlock extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: null
        }
    }

    componentDidMount() {
        fetchData(this.props.path)
            .then(data=>{
                this.setState({ data });
            })
            .catch(data=>{
                this.setState({ data: 'Error loading data' });
            })
    }

    render() {
        return (
            <div>
                {this.state.data ? parseData(this.state.data) : 'loading data...' }
            </div>
        );
    }
}

function fetchData(path) {
    return new Promise((resolve, reject) => {
        fetch(path)
            .then(res=>res.json())
            .then(d=> {
                d ? resolve(d) : reject(d);
            })
            .catch(err=>reject(err))
    });
}

function svgHandler(svg) {
    return <svg dangerouslySetInnerHTML={createMarkup(svg)}></svg>;
}

function createMarkup(data) {
    return {__html: data};
}

function textHandler(textData) {
    return (
        <p className="textData">{textData}</p>
    )
}

function tableHandler(data) {
    let tHead = data.head;
    let tBody = data.body;


    return (
        <div>
            <table className='widget-table'>
                <tbody>
                <tr>{tHead.map(e=><td>{e}</td>)}</tr>
                {tBody.map(i=><tr>
                    {Object.keys(i).map(j=><td>{i[j]}</td>)}
                </tr>)}
                </tbody>
            </table>
        </div>
    )
}

function firstKey(obj) {
    return Object.keys(obj)[0];
}

function parseData(data) {
    if(!data)
        return '';
    switch (firstKey(data)) {
        case 'text':
            return textHandler(data['text']);
        case 'svg':
            return svgHandler(data['svg']);
        case 'head':
            return tableHandler(data);
        default:
            return 'Wrong data'
    }
}

export default DataBlock;