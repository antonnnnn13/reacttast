import React, { Component } from 'react'
import './../App.css'

class TopBar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            opened: true
        }
    }

    handleClick = (event)=>{
        this.props.onClick(event.target.innerText);
    };



    render() {
        return (
            <div className="top-bar">
                <div className="btn-block">
                    <button onClick={this.handleClick}>_</button>
                    <button onClick={this.handleClick}>[]</button>
                    <button onClick={this.handleClick}>X</button>
                </div>
            </div>
        );
    }
}

export default TopBar;