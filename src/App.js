import React, { Component } from 'react';
import './App.css';
import './components/projectFiles'
import Widget from "./components/projectFiles";

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
        widgets: [
            this.widgetFactory(0, 'text'),
            this.widgetFactory(1, 'svg'),
            this.widgetFactory(2, 'table')
        ]
    };
  }

  handleWidgetClose = (id)=>{
    this.setState(prevState=>{
        let prev = [...prevState.widgets];

        prev.forEach((elem, index)=>{
            if(elem.id == id)
                prev.splice(index, 1);
        });


        return {
            widgets: prev
        }
    })
  };

  widgetFactory = (id, task='text')=>{
        return {elem: <Widget id={id} task={task} key={id} onClose={this.handleWidgetClose}/>, id: id}
  };

  render() {
    return (
      <div className="App">
          {this.state.widgets && this.state.widgets.map(widget=>widget.elem)}
      </div>
    );
  }

}


export default App;
